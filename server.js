// import libraries / frameworks
const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv').config()
const cors = require('cors')

// port declaration
const PORT = process.env.PORT || 4000
// server declaration
const app = express()

// import routes module
const userRoutes = require('./api/routes/userRoutes')
const productRoutes = require('./api/routes/productRoutes')

// middleware - json payloads & cors
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())

// Connect MongoDB to server
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});
// Test DB connection
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error'))
db.once('open', () => console.log(`Connected to Database`))

// Routes root url middleware
app.use(`/api/users`, userRoutes)
app.use(`/api/products`, productRoutes)

// server port listener
app.listen(PORT, () => console.log(`Server Connected to port ${PORT}`))