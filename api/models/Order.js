const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
    totalAmount: {
        type: Number,
        // required: [true, `Order total amount is required`]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    userId: {
        type: String,
        required: [true, `User ID is required`]
    },
    items: [ 
        { 
            productId: {
            type: String,
            required: [true, `Product ID is required`]
            },
            quantity: {
                type: Number,
                required: [true, `Product Quantity is required`]
            }
        }
    ]
    
})

module.exports = mongoose.model("Order", orderSchema)