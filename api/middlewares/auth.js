const jwt = require('jsonwebtoken')

// create token for user - sign()
module.exports.createToken = data => {
    let userData = {
        id: data._id,
        email: data.email,
        isAdmin: data.isAdmin
    }

    return jwt.sign(userData, process.env.SECRET_PASS)
}

// verify token
module.exports.verify = (req, res, next) => {
    const requestToken = req.headers.authorization

    // test if req.headers has the token
    if(typeof requestToken == "undefined"){
        res.status(401).send(`Token is missing`)
    }
    else {
        // Remove the 'Bearer ' characters
        const token = requestToken.slice(7, requestToken.length)

        if(typeof token !== "undefined"){
            return jwt.verify(token, process.env.SECRET_PASS, (data, err) => {
                if(err){
                    return res.send({auth: `Authentication Failed!`})
                }
                else {
                    next()
                }
            })
        }
    }
}

// decode JWT payload
module.exports.decode = bearerToken => {
    const token = bearerToken.slice(7, bearerToken.length)

    return jwt.decode(token)
}

// verify Admin access
module.exports.verifyAdmin = (req, res, next) => {
    const requestToken = req.headers.authorization

    // test if req.headers has the token
    if(typeof requestToken == "undefined"){
        res.status(401).send(`Token is missing`)
    }
    else {
        // Remove the 'Bearer ' characters
        const token = requestToken.slice(7, requestToken.length)

        if(typeof token !== "undefined"){
            // test admin status
            const admin = jwt.decode(token).isAdmin
            if(admin){
                return jwt.verify(token, process.env.SECRET_PASS, (err, data) => {
                    if(err){
                        return res.send({auth: `Authentication Failed!`})
                    }
                    else {
                        next()
                    }
                })
            }
            else {
                res.status(403).send(`You are not Admin. Authorization Failed.`)
            }
            
        }
    }
}