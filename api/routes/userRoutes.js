const express = require('express')
const router = express.Router()

// import controller
const userCtrl = require('../controllers/userCtrl')
// import auth middleware
const auth = require('../middlewares/auth')

// routing starts here
// REGISTER User
router.post('/register', async (req,res) => {
    try{
        await userCtrl.registerUser(req.body).then(result => res.status(201).send(result))
    }catch(err) {
        res.status(500).json(err)
    }
})

// GET All User
router.get('/', async (req,res) => {
    try{
        await userCtrl.getAllUsers().then(result => res.status(200).send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// SIGNIN User
router.post('/signin', async (req,res) => {
    try{
        await userCtrl.signInUser(req.body).then(result => res.status(200).send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// SET as Admin User
router.put('/:userId/setAsAdmin', auth.verifyAdmin, async (req,res) => {
    // console.log(req.params.userId)
    console.log()
    try{
        await userCtrl.setAsAdmin(req.params.userId).then(result => res.send(result))
    }catch(err) {
        res.status(500).json(err)
    }
})




module.exports = router