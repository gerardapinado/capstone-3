const express = require('express')
// router declaration
const router = express.Router()

// Product Controller
const productCtrl = require('../controllers/productCtrl')
// middleware
const auth = require('../middlewares/auth')

// routing start here

// CREATE Product - Admin only
router.post('/createProduct', auth.verifyAdmin, async(req,res) => {
    // console.log(req.body)
    try{
        productCtrl.createProduct(req.body).then(result => res.status(201).send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// GET All Products
router.get('/getAllProducts', async(req,res) => {
    try{
        await productCtrl.getAllProducts().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// GET Single Product
router.get('/getProduct/:productId', async(req,res) => {
    try{
        await productCtrl.getProduct(req.params.productId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// UPDATE Product by Id
router.put('/updateProduct/:productId', auth.verifyAdmin, async (req,res) => {
    // console.log(req.body)
    // console.log(req.params.productId)
    try{
        await productCtrl.updateProduct(req.body, req.params.productId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// ARCHIVE Product
router.put('/:productId/archive', auth.verifyAdmin, async(req,res) => {
    try{
        await productCtrl.archiveProduct(req.params.productId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// routing ends here
module.exports = router