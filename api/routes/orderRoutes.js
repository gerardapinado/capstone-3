const { verify } = require("crypto")
const express = require("express")
// const jwt = require('jsonwebtoken')

const router = express.Router()

const orderCtrl = require('../controllers/orderCtrl')
const auth = require('../middlewares/auth')


// CREATE Order
router.post('/checkout', verify, async(req,res) => {
    // decode jwt token
    const userData = auth.decode(req.headers.authorization)
    const userId = userData.id
    if(!userData.isAdmin){
        try{
            await orderCtrl.createOrder(userId, req.body).then(result => res.status(201).send(result))
        }catch(err){
            res.status(500).json(err)
        }
    }
    else {
        res.status(401).send({message: `Unauthorized Request`})
    }
})

// RETRIEVE All Orders
router.get('/orders', async (req,res) => {
    try{
        await orderCtrl.getAllOrders().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

// RETRIEVE User's Order
router.get('/myOrders', async (req,res) => {
    try{
        await orderCtrl.getUserOrder().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})


module.exports = router