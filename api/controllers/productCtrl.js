const cryptoJS = require('crypto-js')
// import model
const Product = require('../models/Product')
// import middleware
const auth = require('../middlewares/auth')


// CREATE Product
module.exports.createProduct = async (reqBody) => {
    const {name,description,price} = reqBody
    
    let newProduct = new Product({
        name: name,
        description: description,
        price: price
    })

    return await newProduct.save().then((result,err) => result ? result : err)  
}

// GET All Products
module.exports.getAllProducts = async() => {
    return await Product.find().then(result => result)
}

// GET Single Product
module.exports.getProduct = async productId => {
    return await Product.findById(productId).then(result => result)
}

// UPDATE Product by Id
module.exports.updateProduct = async (reqBody, productId) => {
    try{
        return await Product.findByIdAndUpdate(productId, {$set: reqBody}, {new:true}).then(result => result)
    }catch(err){
        console.log(err)
        console.log(`awit`)
    }
    
}

// ARCHIVE Product by Id
module.exports.archiveProduct = async (productId) => {
    return await Product.findByIdAndUpdate(productId, {$set: {isActive:false}}).then(result => result ? true : false)
}