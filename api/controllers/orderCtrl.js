const Order = require('../models/Order')
const Product = require('../models/Product')

// CREATE Order
module.exports.createOrder = async (userId, reqBody) => {
    try{
        /*
        let totalAmount=0;
        
        reqBody.items.forEach(data => {
            console.log(data.productId)
            
            Product.findById(data.productId).then(result => { 
                console.log( `price ${result.price}`)
                console.log(`qty ${data.quantity}`)

                totalAmount += result.price * data.quantity
                console.log(totalAmount)
            })
        })
        console.log(totalAmount)
        */

        let newOrder = new Order({
            userId: userId,
            items: reqBody.items
        })

        return await newOrder.save().then((result,err) => result ? result : err)

    }catch(err){
        console.log(err)
    }
}

// RETRIEVE All Orders
module.exports.getAllOrders = async () => {
    return await Order.find().then(result => result)
}

// RETRIEVE User's Order
module.exports.getUserOrder = async () => {
    return await Order.find().then(result => result)
}