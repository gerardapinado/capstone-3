// import crypto-js / encrypting library
const CryptoJS = require('crypto-js')

// import User model
const User = require('../models/Users')

// import middleware/auth function
const {createToken} = require('../middlewares/auth')

// Creating Controllers starts here


// REGISTER User
module.exports.registerUser = async (reqBody) => {
    console.log(reqBody)
    try{
    const {email, password} = reqBody

    
    const newUser = new User({
        email: email,
        password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
    })

    console.log(newUser)

    return await newUser.save().then(result => {
        // console.log(result)
        if(result){
            return true
        }
        else {
            if(result == null){
                return false
            }
        }
    })
    }catch(err){
        console.log(err)
    }
}

// GET All User
module.exports.getAllUsers = async() => {
    return await User.find().then(result => result)
}

// SIGNIN User
module.exports.signInUser = async (reqBody) => {
    return await User.findOne({email: reqBody.email}).then((result, err) => {
        if(result == null){
            return {message: `User does not exist`}
        }
        else {
            if(result !== null){
                const decryptPass = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)
                
                // check if password is match
                // return the JWT token if successful
                if(reqBody.password == decryptPass){
                    return {token: createToken(result)}
                }
                else {
                    return {auth: `Incorrect Password. Authentication Failed!`}
                }
            }
            else {
                return err
            }
        }
    })
}

// SET as Admin User
module.exports.setAsAdmin = async (id) => {
    try{
        return await User.findByIdAndUpdate(id, {$set: {isAdmin:true}}, {new:true}).then((result,err) => result ? true : err)
    }catch(err){
        console.log(err)
        console.log(`awit`)
    }
}