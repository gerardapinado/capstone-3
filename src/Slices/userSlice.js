import { createSlice } from '@reduxjs/toolkit'

const initialValue = null

export const userSlice = createSlice({
    name: "user",
    initialState: {
        value: initialValue
    },
    reducers: {
        login: state => {
            state.value = true
        },
        logout: state => {
            state.value = initialValue
        }
    }
})

export const {login, logout} = userSlice.actions
export default userSlice.reducer
