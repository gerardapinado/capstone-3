export const productsData = {
    mapleSeries: {
        banner: {
            imgBanner: "https://d2bghjaa5qmp6f.cloudfront.net/resize/images/product-image/Pearl-FFXM1412-A.jpg",
            name: "Championship Maple",
            group: "FFXM Snare Drums",
            description: "No other Snare drum has changed the face of marching competition like the FFX Championship Maple Series. Used by today's top marching ensembles, these drums have won more than their share of trophies. Featuring the OneTouch Snare Strainer system in combination with a 6 ply 100% Maple free-floating shell and a super lightweight aircraft aluminum frame, FFXM Snare Drums are a force to be reckoned with."
        },
        details: {
            sizesDesc: "Pearl's FFXM Championship Maple Snare Drums are available in the following sizes",
            sizes: ["14\"x12\" - 14.5 lbs", "13\"x11\" - 13.2 lbs"],
            build1: "Polished Aluminum Hardware is standard, while Chrome Hardware is available via special order.",
            build2: "",
            finishDesc: "Stock wrap finishes available are:",
            finishes: ["#33 Pure White", "#46 Midnight Black", "#450 Silver Sparkle"]
        }
    }
}