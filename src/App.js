import { BrowserRouter, Routes, Route } from "react-router-dom";

import Navbar from "./components/Navbar";
import Footer from "./components/Footer";

import Home from "./pages/Home";
import Register from "./pages/Register"
import Login from "./pages/Login"
import Admin from "./pages/Admin";
import Product from "./pages/Product";
import Cart from "./pages/Cart";

// import { useReducer } from "react";
// import { initialState, reducer} from './reducer/userReducer'
// import { UserProvider } from "./UserContext";

function App() {
  // const [state,dispatch] = useReducer(reducer, initialState)

  return (
    // <UserProvider value={{state, dispatch}}>
      <BrowserRouter>
      <Navbar/>
        <Routes>
          <Route path='/' element={<Home/>}/>
          <Route path='/register' element={<Register/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route path='/logout' element={<Login/>}/>
          <Route path='/admin' element={<Admin/>}/>
          <Route path='/product' element={<Product/>}/>
          <Route path='/cart' element={<Cart/>}/>
        </Routes>
      {/* <Footer/> */}
      </BrowserRouter>
    // </UserProvider>
    
  )
}

export default App;
