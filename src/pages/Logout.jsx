import React from 'react'
import { useContext, useEffect } from 'react'
import UserContext from './../UserContext'
import Navigate, { useNavigate } from 'react-router-dom'

export default function Logout() {
    const { state, dispatch } = useContext(UserContext)
    const navigate = useNavigate

	useEffect( () => {

 		localStorage.clear()
        
        dispatch({type: "USER", payload: null})

        // navigate('/login')
	}, [])

	return( <Navigate to="/login" replaced/> )
}
