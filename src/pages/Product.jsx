import React, { useEffect, useState } from 'react'
import { useSearchParams } from 'react-router-dom';
import ProductBanner from '../components/ProductBanner';
import ProductDetails from '../components/ProductDetails';

export default function Product() {
  const [searchParams] = useSearchParams()

  const [product, setProduct] = useState({})

  useEffect(() => {
    window.scrollTo(0, 0);
  })

  useEffect(() => {
    fetch(`https://stormy-hollows-92875.herokuapp.com/api/products/getProduct/${searchParams.get('_id')}`)
    .then(response => response.json())
    .then(response => {
      setProduct(response)
      console.log(response)
    })
  },[])

  return (
    <section id="product-content">
        <ProductBanner key={product._id} productProp={product}/>
        <div className='product-container'>
        <ProductDetails key={product._id} productProp={product}/>
        </div>
    </section>
  )
}
