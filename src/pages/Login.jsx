import React, { useEffect, useState } from 'react'
import './styles/register.css'
import { useNavigate } from 'react-router-dom'
// import UserContext from '../UserContext'

import { useDispatch } from 'react-redux'
import { login } from '../Slices/userSlice'

export default function Login() {
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [isDisabled, setIsDisabled] = useState(true)
  const navigate = useNavigate()

  const dispatch = useDispatch()
  // const {state, dispatch} = useContext(UserContext)

  useEffect(() => {
		if(email !== "" && password !== ""){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, password])

  function loginUser(e) {
    e.preventDefault()

    fetch('https://stormy-hollows-92875.herokuapp.com/api/users/signin', {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(response => response.json())
    .then(response => {
      if(response){
        localStorage.setItem('token', response.token)
        const token = localStorage.getItem('token')

        // GET the user details. isAdmin property
        // fetch . then ( setUser using Redux)
        fetch('https://stormy-hollows-92875.herokuapp.com/api/users/profile', {
          method: "GET",
          headers: {
            "Authorization": `Bearer ${token}`
          }
        })
        .then(response => response.json())
        .then(response => {
          localStorage.setItem('admin', response.isAdmin)
          dispatch(login())
        })

        setEmail("")
        setPassword("")
        navigate('/')
      }
      else {
        alert('Incorrect Credentials')
      }
    })
  }

  return (
    <div className='form-container bg-light p-3'>
      <h3 className='my-4'>Login Account</h3>
      <hr />

      <form onSubmit={e => loginUser(e)} id='login-form' className='my-4'>

        <div class="form-floating mb-4">
          <input onChange={e => setEmail(e.target.value)} value={email} type="email" class="form-control" id="email" placeholder="email@example.com"/>
          <label for="floatingInput">Email address</label>
        </div>

        <div class="form-floating mb-4">
          <input onChange={e => setPassword(e.target.value)} value={password} type="password" class="form-control" id="password" placeholder="Password"/>
          <label for="password">Password</label>
        </div>

        <div class="d-grid gap-2 col-6 mx-auto mt-4">
          <button class="btn btn-outline-dark btn-lg" type="submit" disabled={isDisabled} >Login</button>
        </div>

      </form>

      <hr />
      <p className='text-center mt-4'>Don't Have an Account? <a href="/register">Register Here</a></p>
    </div>
  )
}
