import React from 'react'
// import ProductCard from '../components/ProductCard'
import Banner from '../components/Banner'
// import CarouselBanner from './../components/CarouselBanner'
import { Fragment } from 'react'

// import UserContext from '../UserContext'
import { useState,useEffect } from 'react'

import {useDispatch} from 'react-redux'
import {login} from './../Slices/userSlice'
import ProductCardNew from '../components/ProductCardNew'

export default function Home() {
//   const { dispatch } = useContext(UserContext)
	const dispatch = useDispatch()

  const token = localStorage.getItem('token')
  const [products, setProducts] = useState([])

  useEffect( () => {
		
			fetch(`https://stormy-hollows-92875.herokuapp.com/api/products/getAllProducts`, {
				method: "GET",
				headers:{
					"Authorization": `Bearer ${token}`
				}
			})
			.then(response => response.json())
			.then(response => {

				if(token !== null){
					// dispatch({type: "USER", payload: true})
					dispatch(login())
				}
				
				setProducts(
					response.map(product => {
						console.log(product)
						return <ProductCardNew key={product._id} productProp={product}/>
					})
				)
			})
		

	}, [])

  return (
    <Fragment>
		<Banner/>
      <div className="container">
        <div className="row">
          {products}
        </div>
		{/* <ProductCardNew/> */}
      </div>
    </Fragment>
  )
}
