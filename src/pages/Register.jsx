import React, { useEffect, useState } from 'react'
import './styles/register.css'
import { useNavigate } from 'react-router-dom'


export default function Register() {

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [rePassword, setRePassword] = useState("")
  const [isDisabled, setIsDisabled] = useState(true)
  const navigate = useNavigate()

  useEffect( ()=>{
    if(email!=="" && password!=="" && rePassword!==""){
      if(password===rePassword){
        setIsDisabled(false)
      }
    }
    else {
      setIsDisabled(true)
    }
  }, [email, password, rePassword])

  function handleSubmit(e){
    e.preventDefault()

    fetch('https://stormy-hollows-92875.herokuapp.com/api/users/register', {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(response => response.json())
    .then(response => {
      if(response){
        alert('Registration Successful.')
        navigate('/login')
      }
      else {
        alert('Registration Failed. Please try again.')
      }
    })
  }

  return (
    <div className='form-container bg-light p-3'>
      <h3 className='my-4'>CREATE AN ACCOUNT</h3>
      <hr />

      <form onSubmit={e => handleSubmit(e)} id='register-form' className='my-4'>
        <div className="form-group form-floating mb-4">
          <input onChange={e => setEmail(e.target.value)} value={email} type="email" className="form-control" id="email" placeholder="email@example.com"/>
          <label for="floatingInput">Email address</label>
        </div>

        <div className="form-group form-floating mb-4">
          <input onChange={e => setPassword(e.target.value)} value={password} type="password" className="form-control" id="password" placeholder="Password"/>
          <label for="password">Password</label>
        </div>

        <div className="form-group form-floating mb-4">
          <input onChange={e => setRePassword(e.target.value)} value={rePassword} type="password" className="form-control" id="repassword" placeholder="Confirm Password"/>
          <label for="repassword">Confirm Password</label>
        </div>

        <div className="d-grid gap-2 col-6 mx-auto mt-4">
          <button className="btn btn-outline-secondary" type="submit" disabled={isDisabled} >Register</button>
        </div>
      </form>

      <hr />
      <p className='text-center mt-4'>Have Already an Account? <a href="/login">Login Here</a></p>
    </div>
  )
}
