import React, { useState } from 'react'
import { Link } from 'react-router-dom'

export default function Cart() {
  const [qtyCounter, setCounter] = useState(0)


  return (
    <section>
      <div className='text-center p-5 border'>
        <p className='text-secondary'> <Link className='text-decoration-none text-dark' to={'/'}>Home</Link> / Cart</p>
        <h1>Cart</h1>
      </div>
      <div className='container border' >
        <div className="row">

          <div className='item-corner col-7'>
            <table>
              <thead>
                <tr className='text-center'>
                  <th></th>
                  <th>Product</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Subtotal</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <img className='img-fluid w-25 h-25 border p-1' src="https://d2bghjaa5qmp6f.cloudfront.net/resize/images/product-image/Pearl-FFXM1412-A.jpg" alt="" />
                  </td>
                  <td>
                    <p>Championship Maple FFX Snare Drum</p>
                    <p>PEARL 14"x12"</p>
                  </td>
                  <td>694.99</td>
                  <td className='d-flex'>
                    <button className='btn btn-secondary p-2 mx-3'>+</button>
                    <p className='border p-2 text-center'>{qtyCounter}</p>
                    <button className='btn btn-secondary p-2 mx-3'>-</button>
                  </td>
                  <td>694.99</td>
                </tr>
              </tbody>
            </table> 
          </div>

          <div className="cart-summary col-5">
            <h3 className='py-4'>Cart Summary</h3>
            <hr />
            <div className='d-flex justify-content-between py-3'>
              <p className='fw-bold'>Subtotal</p>
              <p>$ 649.99</p>
            </div>
            <hr />
            <div className='d-flex justify-content-between py-3'>
              <p className='fw-bold'>Shipping</p>
              <div>
                <form className="form-check text-end ">
                  <label className='px-3' for='free'>Free Shipping</label>
                  <input className='shipping-radio' type="radio" name="shipping-option" id="free" checked/>
                  <br />
                  <label className='px-3' for='flat'>Flat Rate: $10</label>
                  <input className='shipping-radio' type="radio" name="shipping-option" id="flat" value={10}/>
                  <br />
                  <label className='px-3' for='pickup'>Pickup: $15</label>
                  <input className='shipping-radio' type="radio" name="shipping-option" id="pickup" value={15}/>
                </form>
              </div>
            </div>
            <hr />
            <div className='d-flex justify-content-between py-3'>
              <p className='fw-bold'>Total</p>
              <p>$ 649.99</p>
            </div>
            <div className="d-grid gap-2 my-5">
              <button className='btn btn-dark btn-lg'>Proceed to Checkout</button>
            </div>
          </div>
        </div>
        
      </div>
    </section>
    
  )
}
