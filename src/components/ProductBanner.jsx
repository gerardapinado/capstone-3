import React, { Fragment } from 'react'
import { productsData } from '../mockDB/mockData'

export default function ProductBanner({productProp}) {
    const {imgBanner,name,group, description} = productsData.mapleSeries.banner
    const {img} = productProp
  return (
    <Fragment>
        <div className='product-banner'>
            <img className='product-img img-fluid w-75 d-block mx-auto' src={img} alt="product-img" />
        </div>
        <div className='after-banner'>
                <h4 className='category fw-light'>{name}</h4>
                <h2 className='product-group'>{group}</h2>
                <p className='product-desc'>{description}</p>
        </div>
    </Fragment>
  )
}
