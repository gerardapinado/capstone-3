import React from 'react'
import { Fragment } from 'react'
import { useNavigate } from 'react-router-dom'

export default function ProductCardNew({productProp}) {

    const {_id, name, description, price, size, img} = productProp
    const navigate = useNavigate()

    const handleBtn = () => {
        navigate(`/product?_id=${_id}`)
    }

  return (
    <Fragment>
        <div className="col-6 my-5">
            <div className="d-flex">
                <img className='img-fluid w-50 h-50 mx-3 border border-dark p-3' src={img} alt="product-item"/>
                
                <div className='d-flex flex-column justify-content-between py-4'>
                    <h5>{name}</h5>
                    <h6>PEARL {size}</h6>
                    <p>{description}</p>
                    <p>$ {price}</p>
                    <button className='btn btn-dark' onClick={handleBtn}>Learn More</button>
                </div>
            </div>
        </div>
    </Fragment>
  )
}
