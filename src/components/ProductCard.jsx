import React from 'react'
import { Fragment } from 'react'

export default function ProductCard({productProp}) {
    
    const {name, description, price} = productProp

  return (
    <Fragment>
        <div className="card col-3 m-3 " style={{width: '18rem'}}>
            <a href='#' className='text-decoration-none text-dark'>
            <img src="https://voluptart.org/wp-content/uploads/2019/08/Homenaje-a-Miro-300x300.jpg" class="card-img-top" alt="product-img"/>
            <div className="card-body">
                <h5 class="card-title">{name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">Price: ${price}</h6>
                <p className="card-text">{description}.</p>
            </div>
            </a>
        </div>
    </Fragment>
  )
}
