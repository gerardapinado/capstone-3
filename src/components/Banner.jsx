import React from 'react'

export default function Banner() {
  return (
    <div>
      <img className='img-fluid' src='https://images.squarespace-cdn.com/content/v1/5aff887996d455ca53da1056/1531457183358-AXKLF1B8G6R71ITERKQM/Remo_Chino+Hills_FINAL.jpg?format=2500w' alt='snare-line'/>
      <h1 className='percussion-h1 text-center text-white fw-bold'>PERCUSSION.</h1>
    </div>
  )
}
