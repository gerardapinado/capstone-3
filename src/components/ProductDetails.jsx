import React, { useEffect } from 'react'
import { Link, useNavigate, useSearchParams } from 'react-router-dom'
import { productsData } from '../mockDB/mockData'

export default function ProductDetails({productProp}) {
    const {_id, name, description, price, size, img} = productProp
    const {sizesDesc, sizes, build1, build2, finishDesc, finishes} = productsData.mapleSeries.details
    const navigate = useNavigate()

    

  return (
    <div className='product-details'>
        <div className="container">
        <div className="row">
            <div className="col mx-5 p-5">
            {/* Fetch from DB */}
            <img className='img-fluid w-100 h-100' src={img} alt="product-img" />
            </div>
            <div className="col mt-5 mx-5">
            <h3>{name}</h3>
            <h5 className='fw-light'>PEARL {size}</h5>
            <p className=''>Price: $ {price}</p>
            <hr />
            <div className='pe-5 me-5'>
                <p>Pearl's FFXM Championship Maple Snare Drums are available in the following sizes:</p>
                <ul>
                <li className='list-unstyled'>14"x12" - 14.5 lbs</li>
                <li className='list-unstyled'>13"x11" - 13.2 lbs</li>
                </ul>
                <p>Polished Aluminum Hardware is standard, while Chrome Hardware is available via special order.</p>
                <p>Stock wrap finishes available are:</p>
                <ul>
                <li className='list-unstyled'>#33 Pure White</li>
                <li className='list-unstyled'>#46 Midnight Black</li>
                <li className='list-unstyled'>#450 Silver Sparkle</li>
                </ul>
                <div >
                <button className='btn btn-outline-dark w-50 me-3'>Add to Cart</button>
                <button className='btn btn-dark w-25'><Link className='text-light text-decoration-none' to={`/cart`}> Buy Now </Link> </button>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
  )
}
