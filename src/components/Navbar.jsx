import React, { useContext } from 'react'
// import UserContext from '../UserContext'
import { Fragment } from 'react'
import { Navigate, useNavigate } from 'react-router-dom'
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import {useSelector} from 'react-redux'
import { Badge } from '@mui/material';
import { Link } from 'react-router-dom';


export default function Navbar() {

  // const {state, dispatch} = useContext(UserContext)
  // console.log(`Navbar ${state}`)
  const user = useSelector(state => state.user.value)

  const navigate = useNavigate()

  const logout = () => {
    localStorage.clear()
    navigate('/login')
    window.location.reload(false)
  }

  const NavLinks = () => {
    if(user == true){
			return(
				<Fragment>
					<li className="nav-item">
            <a className="nav-link btn btn-secondary rounded-pill px-4 text-light" href='#' onClick={logout}>Logout</a>
          </li>
				</Fragment>
			)
		} else {
			return(
				<Fragment>
				    <li className="nav-item mx-3">
              <a className="nav-link btn btn-secondary rounded-pill px-4 text-light" href="/login">Login</a>
            </li>
            <li className="nav-item">
              <a className="nav-link btn btn-secondary rounded-pill px-4 text-light" href="/register">Register</a>
            </li>
				</Fragment>
			)
		}
  }

  return (
    <Fragment>

    <nav className="navbar navbar-expand-lg navbar-light bg-light" >
      <div className="container-fluid">
        <a className="navbar-brand" href="/">
          <img className="img-fluid m-1" src="https://upload.wikimedia.org/wikipedia/commons/e/ea/Pearl_drum_logo.png" alt="pearl-logo" width="100" height="30"/>
        </a>
        
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">

          <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <a className="nav-link mx-3 btn btn-outline-secondary rounded-pill px-4" aria-current="page" href="/">Home</a>
            </li>
            <form className="d-flex">
              <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
              <button className="btn btn-outline-secondary" type="submit">Search</button>
            </form>
            <div className="d-flex align-items-center mx-3" badgeContent={2}>
            <Link className='text-dark' to={'/cart'}>
              <Badge badgeContent={5} color="primary">
                
                  <ShoppingCartOutlinedIcon/>
            
              </Badge>
            </Link>
            </div>
            <NavLinks/>
          </ul>
        </div>
      </div>
    </nav>
    </Fragment>
  )
}
